/**
 * 应收账款 - 主界面
 * 
 * @author 艾格林门信息服务（大连）有限公司
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.Funds.RvMainForm", {
  extend: "PSI.AFX.Form.MainForm",

  /**
   * @override
   */
  initComponent() {
    const me = this;

    const modelName = "PSIModel.PSI.Funds.RvMainForm.CACategory";

    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "name"]
    });

    PCL.apply(me, {
      tbar: [{
        xtype: "displayfield",
        margin: "5 0 0 0",
        value: "往来单位："
      }, {
        cls: "PSI-toolbox",
        xtype: "combo",
        id: "comboCA",
        queryMode: "local",
        editable: false,
        valueField: "id",
        store: PCL.create("PCL.data.ArrayStore", {
          fields: ["id", "text"],
          data: [["customer", "客户"],
          ["supplier", "供应商"]]
        }),
        value: "customer",
        listeners: {
          select: {
            fn: me._onComboCASelect,
            scope: me
          }
        }
      }, {
        xtype: "displayfield",
        margin: "5 0 0 0",
        value: "分类"
      }, {
        cls: "PSI-toolbox",
        xtype: "combobox",
        id: "comboCategory",
        queryMode: "local",
        editable: false,
        valueField: "id",
        displayField: "name",
        store: PCL.create("PCL.data.Store", {
          model: modelName,
          autoLoad: false,
          data: []
        })
      }, {
        id: "editQueryLabel",
        margin: "5 0 0 0",
        xtype: "displayfield",
        value: "客户 "
      }, {
        cls: "PSI-toolbox",
        id: "editCustomerQuery",
        xtype: "psi_customerfield",
        width: 200,
        showModal: true
      }, {
        cls: "PSI-toolbox",
        id: "editSupplierQuery",
        xtype: "psi_supplierfield",
        hidden: true,
        width: 200,
        showModal: true
      }, {
        xtype: "checkbox",
        boxLabel: "只显示有未收的记录",
        inputValue: "1",
        id: "editQueryHasBalance",
        margin: "5 0 0 0",
        listeners: {
          change: {
            fn() {
              me._onQuery();
            },
            scoep: me
          }
        }
      }, " ", "-", {
        text: "查询",
        iconCls: "PSI-tb-query",
        handler: me._onQuery,
        scope: me
      }, {
        text: "清空查询条件",
        handler: me._onClearQuery,
        scope: me
      }, "-", {
        iconCls: "PSI-tb-help",
        text: "指南",
        handler() {
          me.focus();
          window.open(me.URL("Home/Help/index?t=receivable"));
        }
      }, "-", {
        iconCls: "PSI-tb-close",
        text: "关闭",
        handler() {
          me.closeWindow();
        }
      }, {
        // 空容器，只是为了撑高工具栏
        xtype: "container", height: 28,
        items: []
      }].concat(me.getShortcutCmp()),
      layout: "border",
      border: 0,
      items: [{
        region: "north",
        height: 1,
        border: 0,
      }, {
        region: "center",
        layout: "fit",
        border: 0,
        items: [me.getRvGrid()]
      }, {
        region: "south",
        layout: "border",
        border: 0,
        split: true,
        height: "50%",
        items: [{
          region: "center",
          border: 0,
          layout: "fit",
          items: [me.getRvDetailGrid()]
        }, {
          region: "east",
          layout: "fit",
          border: 0,
          width: "40%",
          split: true,
          items: [me.getRvRecordGrid()]
        }]
      }]
    });

    me.callParent(arguments);

    me._onComboCASelect();
  },

  /**
   * 快捷访问
   * 
   * @private
   */
  getShortcutCmp() {
    return ["->",
      {
        cls: "PSI-Shortcut-Cmp",
        labelWidth: 0,
        emptyText: "快捷访问",
        xtype: "psi_mainmenushortcutfield",
        width: 90
      }
    ];
  },

  /**
   * @private
   */
  getRvGrid() {
    const me = this;
    if (me._rvGrid) {
      return me._rvGrid;
    }

    const modelName = "PSIModel.PSI.Funds.RvMainForm.Rv";

    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "caId", "code", "name", "rvMoney",
        "actMoney", "balanceMoney"]
    });

    const store = PCL.create("PCL.data.Store", {
      model: modelName,
      pageSize: 20,
      proxy: {
        type: "ajax",
        actionMethods: {
          read: "POST"
        },
        url: me.URL("Home/Funds/rvList"),
        reader: {
          root: 'dataList',
          totalProperty: 'totalCount'
        }
      },
      autoLoad: false,
      data: []
    });

    store.on("beforeload", () => {
      PCL.apply(store.proxy.extraParams, {
        caType: PCL.getCmp("comboCA").getValue(),
        categoryId: PCL.getCmp("comboCategory").getValue(),
        customerId: PCL.getCmp("editCustomerQuery").getIdValue(),
        supplierId: PCL.getCmp("editSupplierQuery").getIdValue(),
        hasBalance: PCL.getCmp("editQueryHasBalance").getValue() ? 1 : 0
      });
    });

    me._rvGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-FC",
      viewConfig: {
        enableTextSelection: true
      },
      bbar: ["->", {
        xtype: "pagingtoolbar",
        border: 0,
        store: store
      }],
      columnLines: true,
      columns: [{
        header: "编码",
        dataIndex: "code",
        menuDisabled: true,
        sortable: false
      }, {
        header: "名称",
        dataIndex: "name",
        menuDisabled: true,
        sortable: false,
        width: 300
      }, {
        header: "应收金额",
        dataIndex: "rvMoney",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 160
      }, {
        header: "已收金额",
        dataIndex: "actMoney",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 160
      }, {
        header: "未收金额",
        dataIndex: "balanceMoney",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 160
      }],
      store: store,
      listeners: {
        select: {
          fn: me._onRvGridSelect,
          scope: me
        }
      }
    });

    return me._rvGrid;
  },

  /**
   * @private
   */
  getRvParam() {
    const item = this.getRvGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return null;
    }

    const rv = item[0];
    return rv.get("caId");
  },

  /**
   * @private
   */
  _onRvGridSelect() {
    const me = this;

    me.getRvRecordGrid().getStore().removeAll();
    me.getRvRecordGrid().setTitle(me.formatGridHeaderTitle("收款记录"));

    me.getRvDetailGrid().getStore().loadPage(1);
  },

  /**
   * @private
   */
  getRvDetailGrid() {
    const me = this;
    if (me._rvDetailGrid) {
      return me._rvDetailGrid;
    }

    const modelName = "PSIModel.PSI.Funds.RvMainForm.RvDetail";

    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "rvMoney", "actMoney", "balanceMoney",
        "refType", "refNumber", "bizDT", "dateCreated"]
    });

    const store = PCL.create("PCL.data.Store", {
      model: modelName,
      pageSize: 20,
      proxy: {
        type: "ajax",
        actionMethods: {
          read: "POST"
        },
        url: me.URL("Home/Funds/rvDetailList"),
        reader: {
          root: 'dataList',
          totalProperty: 'totalCount'
        }
      },
      autoLoad: false,
      data: []
    });

    store.on("beforeload", () => {
      PCL.apply(store.proxy.extraParams, {
        caType: PCL.getCmp("comboCA").getValue(),
        caId: me.getRvParam()
      });
    });

    me._rvDetailGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-HL",
      viewConfig: {
        enableTextSelection: true
      },
      header: {
        height: 30,
        title: me.formatGridHeaderTitle("业务单据")
      },
      bbar: ["->", {
        xtype: "pagingtoolbar",
        border: 0,
        store: store
      }],
      columnLines: true,
      columns: [{
        header: "业务类型",
        dataIndex: "refType",
        menuDisabled: true,
        sortable: false,
        width: 120
      }, {
        header: "单号",
        dataIndex: "refNumber",
        menuDisabled: true,
        sortable: false,
        width: 120,
        renderer(value, md, record) {
          if (record.get("refType") == "应收账款期初建账") {
            return value;
          }

          return "<a href='"
            + PSI.Const.BASE_URL
            + "Home/Bill/viewIndex?fid=2004&refType="
            + encodeURIComponent(record
              .get("refType"))
            + "&ref="
            + encodeURIComponent(record
              .get("refNumber"))
            + "' target='_blank'>" + value
            + "</a>";
        }
      }, {
        header: "业务日期",
        dataIndex: "bizDT",
        menuDisabled: true,
        sortable: false
      }, {
        header: "应收金额",
        dataIndex: "rvMoney",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn"
      }, {
        header: "已收金额",
        dataIndex: "actMoney",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn"
      }, {
        header: "未收金额",
        dataIndex: "balanceMoney",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn"
      }, {
        header: "创建时间",
        dataIndex: "dateCreated",
        menuDisabled: true,
        sortable: false,
        width: 140
      }],
      store: store,
      listeners: {
        select: {
          fn: me.onRvDetailGridSelect,
          scope: me
        }
      }
    });

    return me._rvDetailGrid;
  },

  /**
   * @private
   */
  onRvDetailGridSelect() {
    const me = this;

    const grid = this.getRvRecordGrid();
    const item = this.getRvDetailGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      grid.setTitle(me.formatGridHeaderTitle("收款记录"));
      return null;
    }

    const rvDetail = item[0];

    grid.setTitle(me.formatGridHeaderTitle(rvDetail.get("refType")
      + " - 单号: " + rvDetail.get("refNumber") + " 的收款记录"));
    grid.getStore().loadPage(1);
  },

  /**
   * @private
   */
  getRvRecordGrid() {
    const me = this;
    if (me._rvRecordGrid) {
      return me._rvRecordGrid;
    }

    const modelName = "PSIModel.PSI.Funds.RvMainForm.RvRecord";

    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "actMoney", "bizDate", "bizUserName",
        "inputUserName", "dateCreated", "remark"]
    });

    const store = PCL.create("PCL.data.Store", {
      model: modelName,
      pageSize: 20,
      proxy: {
        type: "ajax",
        actionMethods: {
          read: "POST"
        },
        url: me.URL("Home/Funds/rvRecordList"),
        reader: {
          root: 'dataList',
          totalProperty: 'totalCount'
        }
      },
      autoLoad: false,
      data: []
    });

    store.on("beforeload", () => {
      let rvDetail
      const item = me.getRvDetailGrid().getSelectionModel().getSelection();
      if (item == null || item.length != 1) {
        rvDetail = null;
      } else {
        rvDetail = item[0];
      }

      PCL.apply(store.proxy.extraParams, {
        refType: rvDetail == null ? null : rvDetail.get("refType"),
        refNumber: rvDetail == null ? null : rvDetail.get("refNumber")
      });
    });

    me._rvRecordGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-HL",
      viewConfig: {
        enableTextSelection: true
      },
      header: {
        height: 30,
        title: me.formatGridHeaderTitle("收款记录")
      },
      tbar: [{
        text: "录入收款记录",
        iconCls: "PSI-tb-new",
        handler: me._onAddRvRecord,
        scope: me
      }],
      bbar: ["->", {
        xtype: "pagingtoolbar",
        border: 0,
        store: store
      }],
      columnLines: true,
      columns: [{
        header: "收款日期",
        dataIndex: "bizDate",
        menuDisabled: true,
        sortable: false,
        width: 80
      }, {
        header: "收款金额",
        dataIndex: "actMoney",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn"
      }, {
        header: "收款人",
        dataIndex: "bizUserName",
        menuDisabled: true,
        sortable: false,
        width: 80
      }, {
        header: "录入时间",
        dataIndex: "dateCreated",
        menuDisabled: true,
        sortable: false,
        width: 140
      }, {
        header: "录入人",
        dataIndex: "inputUserName",
        menuDisabled: true,
        sortable: false,
        width: 80
      }, {
        header: "备注",
        dataIndex: "remark",
        menuDisabled: true,
        sortable: false,
        width: 150
      }],
      store: store
    });

    return me._rvRecordGrid;
  },

  /**
   * @private
   */
  _onComboCASelect() {
    const me = this;

    const caType = PCL.getCmp("comboCA").getValue();
    if (caType == "customer") {
      PCL.getCmp("editQueryLabel").setValue("客户");
      PCL.getCmp("editCustomerQuery").setVisible(true);
      PCL.getCmp("editSupplierQuery").setVisible(false);
    } else {
      PCL.getCmp("editQueryLabel").setValue("供应商");
      PCL.getCmp("editCustomerQuery").setVisible(false);
      PCL.getCmp("editSupplierQuery").setVisible(true);
    }

    me.getRvGrid().getStore().removeAll();
    me.getRvDetailGrid().getStore().removeAll();
    me.getRvRecordGrid().getStore().removeAll();

    const el = PCL.getBody();
    el.mask(PSI.Const.LOADING);
    me.ajax({
      url: me.URL("Home/Funds/rvCategoryList"),
      params: {
        id: PCL.getCmp("comboCA").getValue()
      },
      callback(options, success, response) {
        const combo = PCL.getCmp("comboCategory");
        const store = combo.getStore();

        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);

          if (store.getCount() > 0) {
            combo.setValue(store.getAt(0).get("id"))
          }

          if (!me._inited) {
            me._onQuery();
            me._inited = true;
          }
        }

        el.unmask();
      }
    });
  },

  /**
   * @private
   */
  _onQuery() {
    const me = this;
    me.getRvDetailGrid().getStore().removeAll();
    me.getRvRecordGrid().getStore().removeAll();
    me.getRvRecordGrid().setTitle(me.formatGridHeaderTitle("收款记录"));

    me.getRvGrid().getStore().loadPage(1);
  },

  /**
   * @private
   */
  _onAddRvRecord() {
    const me = this;
    const item = me.getRvDetailGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("请选择要做收款记录的业务单据");
      return;
    }

    const rvDetail = item[0];

    const form = PCL.create("PSI.Funds.RvRecordEditForm", {
      parentForm: me,
      rvDetail: rvDetail
    })
    form.show();
  },

  /**
   * @private
   */
  refreshRvInfo() {
    const me = this;
    const item = me.getRvGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }
    const rv = item[0];

    me.ajax({
      url: me.URL("Home/Funds/refreshRvInfo"),
      params: {
        id: rv.get("id")
      },
      callback(options, success, response) {
        if (success) {
          const data = me.decodeJSON(response.responseText);
          rv.set("actMoney", data.actMoney);
          rv.set("balanceMoney", data.balanceMoney)
          me.getRvGrid().getStore().commitChanges();
        }
      }
    });
  },

  /**
   * @private
   */
  refreshRvDetailInfo() {
    const me = this;
    const item = me.getRvDetailGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }
    const rvDetail = item[0];

    me.ajax({
      url: me.URL("Home/Funds/refreshRvDetailInfo"),
      params: {
        id: rvDetail.get("id")
      },
      callback(options, success, response) {
        if (success) {
          const data = me.decodeJSON(response.responseText);
          rvDetail.set("actMoney", data.actMoney);
          rvDetail.set("balanceMoney", data.balanceMoney)
          me.getRvDetailGrid().getStore().commitChanges();
        }
      }

    });
  },

  /**
   * @private
   */
  _onClearQuery() {
    const me = this;

    PCL.getCmp("editCustomerQuery").clearIdValue();
    PCL.getCmp("editSupplierQuery").clearIdValue();
    me._onQuery();
  }
});
